# envtmpl #

Looks similar to safekeeper but it is completely different.

(a) uses the golang template format

(b) uses the entire environment

The outcome should be obvious.


# Example #

```
envtmpl  --output=out template.test
```

# LICENSE #

Licensed under MIT license.