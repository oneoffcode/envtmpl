// Copyright (c) 2015 One Off Code LLC
package main

import (
	"flag"

	"bitbucket.org/oneoffcode/envtmpl"
)

var (
	output  = flag.String("output", "", "Output file name. default srcdir/source.go")
	comment = flag.String("c", "//", "comment prefix string")
)

func main() {
	flag.Parse()
	envtmpl.Run(*comment, *output, flag.Args())
}
